## This module enhances media management capabilities by providing essential functionalities:

## Default Media Protection: 
Prevent unauthorized changes to default media by implementing custom permissions that block editing for designated entities. Maintain consistency and integrity across your portal by safeguarding crucial default media assets from unintended modifications.
Go to any media item, there should be a checkbox "Default Media", check it to set the entity as a default media, which will restrict the editing of media for users with "Bypass default media access" permission.

## Media Usability Check:
 Ensure efficient media utilization by enabling the "Check Media Usability" feature. Receive warning messages on the media editing interface, notifying users if the media is currently in use elsewhere within the portal. This proactive approach enhances content management efficiency and prevents accidental modifications to media assets crucial for multiple entities.
 
## Module Installation
From the toolbar, go to Configuration > Content Authoring > Seeds Media
Check the box "Check Media Usability".