<?php

namespace Drupal\seeds_media;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\media\MediaInterface;

/**
 * Provides helper methods for dealing with media entities.
 */
class MediaHelper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * MediaHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Determines the use count of the media entity in all fields.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   *
   * @return int
   *   The number of times this media is used in all fields
   */
  public function mediaUseablity(MediaInterface $media) {
    // Get all entity reference fields that target media entities.
    /** @var \Drupal\field\FieldStorageConfigInterface[] $fields */
    $fields = $this->entityTypeManager->getStorage('field_storage_config')->loadByProperties([
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => 'media',
      ],
    ]);

    // Get each query for each table.
    $queries = [];
    foreach ($fields as $field) {
      $name = $field->getName();
      $entity_type = $field->get('entity_type');
      $table_name = "{$entity_type}__{$name}";
      $query = \Drupal::database()->select($table_name, 'T');
      $query->fields('T', ['entity_id', "{$name}_target_id"]);
      $query->where("{$name}_target_id = :id", [':id' => $media->id()]);
      $queries[] = $query;
    }

    // Begin a union query.
    $union = NULL;
    $union = array_reduce($queries, function ($prev, $curr) {
      if ($prev == NULL) {
        return $curr;
      }
      return $prev->union($curr);
    });

    // Execute the union query.
    $result = $union->execute()->fetchAll();
    return count($result);
  }

}
